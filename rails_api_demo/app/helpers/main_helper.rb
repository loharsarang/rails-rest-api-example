module MainHelper

  def fact(factorial)
    if (factorial==0)
      return 1
    else
      return factorial * fact(factorial - 1)
    end
  end

end
