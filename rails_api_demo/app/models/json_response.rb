class JsonResponse
  @status
  @data
  @exception
  @status_text

  def initialize(status, data, exception, status_text)
    @status, @data, @exception, @status_text = status, data, exception, status_text
  end

end
