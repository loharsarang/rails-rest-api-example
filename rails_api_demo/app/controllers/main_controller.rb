class MainController < ApplicationController
  include MainHelper

  def hello
    render json: "Hello World #{params.inspect}", status: :ok
  end

  def index
    render json: "Index method is called #{params.inspect}", status: :created
    #render json: "Index method is called #{params.inspect}", status: :unprocessable_entity
    #render json: "Index method is called #{params.inspect}", status: :unauthorized
    #render json: "Index method is called #{params.inspect}", status: :created
  end

  def home
    render json: "home page is here #{params.inspect}", status: :ok
  end

  def world
    a = fact 5
    render json: "world #{params.inspect} #{a} ", status: :ok
  end

end
