class UserController < ApplicationController
  # show all user records :path /users
  def index
    users = User.all
    json_response = JsonResponse.new(200, users, "No Exception", "OK")
    render json: json_response
  end

  # show perticular user record by id :path /user/:id
  def show
    id = params[:id]
    user = User.find(id)
    json_response = JsonResponse.new(200, user, "No Exception", "OK")
    render json: json_response.to_json, status: :ok


  rescue Exception => e
    puts "record not found!!"
    json_response = JsonResponse.new(404, user.to_json, e, "not found")
    render json: json_response.to_json, status: :not_found

  end

  #{"param": {"first_name":"Sarang","last_name":"Lohar","birth_date":"1994-01-15","age":23,
  # "email_id":"sarang.lohar","mobile_number":9595599509,"organization_name":"Kanaka",
  # "salary":500000.0,"retired":false,"created_at":"2016-11-28T10:54:18.000Z",
  # "updated_at":"2016-11-28T10:55:37.000Z"}}

  # Create new user record in database :path /user :POST
  def create
    @user = User.new(user_params)
    puts @user

    if @user.save
      json_response = JsonResponse.new(200, @user, "No Exception", "ok")
      render json: json_response.to_json, status: :ok
    else
      json_response = JsonResponse.new(200, @user.errors, "Exception", "conflict")
      render json: json_response, status: :conflict
    end
  end

  # For updating a data get existing record :path /user/:id/edit GET
  def edit
    json_response = JsonResponse.new(200, User.find(params[:id]), "No Exception", "ok")
    render json: json_response, status: :ok
  end

  # update existing user record  :path /user/:id  :PUT
  def update

    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      render json: @user.to_json
    else
      render json: user_params
    end

  end

  # Delete user from database  :path /user/:id :GET
  def destroy
    user = User.find(params[:id])
    user.delete
    render json: user
  end

  private
  # private method to match with model object and verify required parameters
  def user_params
    params.require(:user).permit(:id, :first_name, :last_name, :birth_date, :age, :email_id, :mobile_number, :organization_name, :salary)
  end
end
