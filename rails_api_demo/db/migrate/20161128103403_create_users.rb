class CreateUsers < ActiveRecord::Migration[5.0]
  def up
    create_table :users do |t|

      t.string "first_name", :limit => 25
      t.string "last_name", :limit => 25
      t.date "birth_date"
      t.integer "age", :limit => 2
      t.string "email_id", :limit => 35
      t.decimal "mobile_number", :limit => 10
      t.string "organization_name", :limit => 50
      t.float "salary", :limit => 10
      t.boolean "retired", :default => false
      t.timestamps
      puts "users table created (UP)"
    end

    def down
      drop_table :users
      puts "users table Removed (DOWN)"
    end
  end
end
